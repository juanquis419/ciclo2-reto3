package moneda;

public class ConvertTo {

    private final Double usdToCop = 3974.00;
    private final Double gbpToCop = 5501.78;
    private final Double eurToCop = 4657.42;
    private final Double cadToCop = 3170.31;

    public Double transforma (Moneda cash) {
        if (cash.getValor() >= 0D){

            if (cash.getSimbolo().equals("USD")) {
                return cash.getValor() / this.usdToCop;
    
            } else if (cash.getSimbolo().equals("GBP")) {
                return cash.getValor() / this.gbpToCop;
    
            } else if (cash.getSimbolo().equals("EUR")) {
                return cash.getValor() / this.eurToCop;
    
            } else if (cash.getSimbolo().equals("CAD")) {
                return (cash.getValor() / this.cadToCop);

            }

        }
        
        return -1D;
    }
    
}
