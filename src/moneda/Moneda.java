package moneda;

public class Moneda {
    
    private String simbolo;
    private Double  valor;
    
    public void setValor(Double nuevoValor){
        this.valor = nuevoValor;
    }

    public Double getValor() {
        return this.valor;
    }

    public void setSimbolo(String nuevoSimbolo){
        this.simbolo = nuevoSimbolo;
    }

    public String getSimbolo() {
        return this.simbolo;
    }

    public String toString() {
        return "Usted quiere convertir " + valor + " " + simbolo;
    }

    public Moneda(String elSimbolo, Double elValor) {
        this.simbolo = elSimbolo;
        this.valor   = elValor;
    }

}
