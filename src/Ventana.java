import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JLabel;

import moneda.*;

public class Ventana extends JFrame {

    private javax.swing.JButton btConvert;
    private javax.swing.JButton btSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelResul;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabelUsd;
    private javax.swing.JLabel jLabelEur;
    private javax.swing.JLabel jLabelGbp;
    private javax.swing.JLabel jLabelPrintCopToGbp;
    private javax.swing.JLabel jLabelPrintCopToUsd;
    private javax.swing.JLabel jLabelPrintCopToEur;
    private javax.swing.JLabel jLabelPrintCopToCad;
    private javax.swing.JTextField cjPesos;
    private javax.swing.JLabel jLabelCad;

    public Ventana() {
        this.iniciar();
        
    }

    private void iniciar() {
        
        jLabel1 = new javax.swing.JLabel();
        jLabelResul = new javax.swing.JLabel();
        cjPesos = new javax.swing.JTextField(); // Listo
        jLabel2 = new javax.swing.JLabel();
        btConvert = new javax.swing.JButton();
        btSalir = new javax.swing.JButton();
        jLabelUsd = new javax.swing.JLabel();
        jLabelEur = new javax.swing.JLabel();
        jLabelGbp = new javax.swing.JLabel();
        jLabelCad = new javax.swing.JLabel();
        jLabelPrintCopToUsd = new javax.swing.JLabel();
        jLabelPrintCopToEur = new javax.swing.JLabel();
        jLabelPrintCopToGbp = new javax.swing.JLabel();
        jLabelPrintCopToCad = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setText("Calculadora de conversión de monedas");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(80, 5, 1500, 70); // check

        jLabel2.setText("Pesos (COP) :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(10, 85, 85, 20);

        getContentPane().add(cjPesos);
        cjPesos.setBounds(100, 80, 150, 30); // check

        btConvert.setText("Convertir");
        getContentPane().add(btConvert);
        btConvert.setBounds(270, 80, 100, 30); // Check

        jLabelResul.setText("Resultado");
        getContentPane().add(jLabelResul);
        jLabelResul.setBounds(155, 95, 1500, 70); // check

        btSalir.setText("SALIR");
        getContentPane().add(btSalir);
        btSalir.setBounds(270, 180, 100, 30);

        jLabelCad.setText("Dolares Canadienses (C$)");
        getContentPane().add(jLabelCad);
        jLabelCad.setBounds(10, 150, 250, 14);

        jLabelUsd.setText("Dolares ($)");
        getContentPane().add(jLabelUsd);
        jLabelUsd.setBounds(10, 170, 250, 14);

        jLabelGbp.setText("Libras   (£)");
        getContentPane().add(jLabelGbp);
        jLabelGbp.setBounds(10, 190, 250, 14);

        jLabelEur.setText("Euros    (€)");
        getContentPane().add(jLabelEur);
        jLabelEur.setBounds(10, 210, 250, 14);

        jLabelPrintCopToUsd.setText("COP A USD");
        getContentPane().add(jLabelPrintCopToUsd);
        jLabelPrintCopToUsd.setBounds(170, 170, 100, 14);

        jLabelPrintCopToEur.setText("COP A EUR");
        getContentPane().add(jLabelPrintCopToEur);
        jLabelPrintCopToEur.setBounds(170, 210, 100, 14);

        jLabelPrintCopToGbp.setText("COP A GBP");
        getContentPane().add(jLabelPrintCopToGbp);
        jLabelPrintCopToGbp.setBounds(170, 190, 100, 14);

        jLabelPrintCopToCad.setText("COP A CAD");
        getContentPane().add(jLabelPrintCopToCad);
        jLabelPrintCopToCad.setBounds(170, 150, 100, 14);

        btSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalirActionPerformed(evt);
            }
        });
        
        btConvert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btConvertActionPerformed(evt);
            }
        });

        setSize(new java.awt.Dimension(400, 290));
        setLocationRelativeTo(null);
        setResizable(false);
    }

    private void btSalirActionPerformed(java.awt.event.ActionEvent evt) {                                        
        this.salir();
    }                                       

    private void btConvertActionPerformed(java.awt.event.ActionEvent evt) {                                          
        this.newCurrency();
    }                                          
    
    private void salir(){
        System.exit(0);
    }

    private void newCurrency(){

        Double monto;
        String synCurr[] = {"CAD","USD","EUR","GBP"};
        JLabel labels[]  = {this.jLabelPrintCopToCad, this.jLabelPrintCopToUsd, this.jLabelPrintCopToEur, this.jLabelPrintCopToGbp};

        try {
            monto = Double.parseDouble(cjPesos.getText());

        } catch (Exception e){

            monto = null;
        }

        if (monto == null){
            JOptionPane.showMessageDialog(this, "No es un número vuelva a ingresar el dato");

        } else if (monto < 0) {
            JOptionPane.showMessageDialog(this, "No tiene sentido un valor negativo");

        } else {

            for (int i = 0; i < synCurr.length; i++) {
                Moneda curren = new Moneda(synCurr[i], monto);

                ConvertTo newValue = new ConvertTo();

                labels[i].setText(String.format("%.2f", newValue.transforma(curren)));

            }

        }

    }
    

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

}
